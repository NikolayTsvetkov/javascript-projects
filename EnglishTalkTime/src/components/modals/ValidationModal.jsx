import { Button, Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalFooter } from '@chakra-ui/react';
import { useValidation } from '../../contexts/ValidationCOntext';
import { useEffect, useState } from 'react';

function ValidationModal() {
    const { errors, clearErrors } = useValidation();
    const [isModalOpen, setIsModalOpen] = useState(false);


    useEffect(() => {
        if (errors.length > 0) {
            setIsModalOpen(true);
        }
    }, [errors]);

    const handleCloseModal = () => {
        setIsModalOpen(false);
        clearErrors();
    };

    return (
        <div>

            <Modal isOpen={isModalOpen} onClose={handleCloseModal}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Error</ModalHeader>
                    <ModalBody>
                        {errors.map((error, index) => (
                            <div key={index}>{error}</div>
                        ))}
                    </ModalBody>
                    <ModalFooter>
                        <Button colorScheme="blue" onClick={handleCloseModal}>
                            Close
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>

            <button onClick={() => addError('This is an example error.')}>
                Trigger Error
            </button>

            <button onClick={clearErrors}>Clear Errors</button>
        </div>
    );
}

export default ValidationModal;

import React, { useEffect, useState } from 'react';
import { Box, Flex, Heading, Text } from '@chakra-ui/react';
import { useAuth } from '../../contexts/AuthContext';
//import Calendar from '../Calendar/Calendar';

const MainPage = () => {
    const [activeUser, setActiveUser] = useState(null);
    const { currentUser } = useAuth();
    // console.log(currentUser)
    useEffect(() => {
        const checkActiveUser = async () => {

            if (currentUser) {
                setActiveUser(currentUser);
            }
        };

        checkActiveUser(activeUser);
    }, [currentUser]);
    return (
        <Box
            bgGradient="linear(to-r, purple.500, gray.800)"
            py={20}
            px={10}
            textAlign="center"
            borderRadius="md"
            boxShadow="xl"
            w="100%"
        >
            <Flex direction="column" align="center" maxW="md" mx="auto">
                <Heading as="h1" size="xl" color="white" mb={4}>
                    Main Pahe Header
                </Heading>
                <Text color="white" fontSize="lg">
                    Main Page Text
                    {/* <Calendar /> */}
                </Text>
            </Flex>
        </Box>

    );
};

export default MainPage;

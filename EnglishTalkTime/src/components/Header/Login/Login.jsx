import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, Button, Center, Input, Stack, extendTheme } from "@chakra-ui/react";
// import { loginUser } from '../../services/auth.service';
// import { useAuth } from '../../contexts/AuthContext';
// import { useValidation } from '../../contexts/ValidationCOntext';
import { updateCurrentUser } from '@firebase/auth';
import { loginUser } from '../../../services/auth.service';
import { useAuth } from '../../../contexts/AuthContext';
import { useValidation } from '../../../contexts/ValidationCOntext';

const theme = extendTheme({
  colors: {
    purple: {
      900: '#4A0072',
    },
    grey: {
      600: '#666666',
    },
    black: '#000000',
  },
});

export default function Login() {
  const [credentials, setCredentials] = useState({ email: '', password: '' });
  const { handleSetCurrentUser } = useAuth();
  const navigate = useNavigate();
  const { errors, addError, clearErrors } = useValidation();

  const handleLogin = async () => {
    try {
      const user = await loginUser(credentials.email, credentials.password);
      handleSetCurrentUser(user?.userObj);
      // handleSetCurrentUser(user);
      navigate('/');
    } catch (error) {
      console.log('Login error:', Object.keys(error), error.code, error.name, error.customData);

      addError(error.code)

    }
  };

  const handleInputChange = e => {
    const { name, value } = e.target;
    setCredentials(prevCredentials => ({
      ...prevCredentials,
      [name]: value
    }));
  };
  const { currentUser } = useAuth()

  return (
    <Center h="100vh">
      <Box
        bg="gray.800"
        w="400px"
        p="6"
        rounded="md"
        boxShadow="xl"
        color="white"
        theme={theme}
      >
        <Stack spacing={5}>
          <Input
            value={credentials.email}
            onChange={handleInputChange}
            name="email"
            type="email"
            placeholder="Username"
            size="sm"
            focusBorderColor="purple.400"
            variant="flushed"
            bg="gray.500"
            color="white"
            _hover={{ bg: 'gray.600' }}
            _focus={{ bg: 'gray.700', border: '2px solid purple', boxShadow: '0 0 0 1px white' }}

          />

          <Input
            value={credentials.password}
            onChange={handleInputChange}
            name="password"
            type="password"
            placeholder="Password"
            size="sm"
            focusBorderColor="purple.400"
            variant="flushed"
            bg="gray.500"
            color="white"
            _hover={{ bg: 'gray.600' }}
            _focus={{ bg: 'gray.700', border: '2px solid purple', boxShadow: '0 0 0 1px white' }}
          />

          <Center>
            <Button
              colorScheme="purple"
              size="lg"
              width="50%"
              alignItems="center"
              onClick={handleLogin}
            >
              Sign in
            </Button>
          </Center>

          <Center>
            <Button
              colorScheme="purple"
              size="lg"
              width="50%"
              onClick={() => navigate('/signup')}
            >
              Sign up
            </Button>
          </Center>
        </Stack>
      </Box>
    </Center>
  );
}



// import { Card, Center, CardBody, Stack, Button, Input } from "@chakra-ui/react";
// import "./Login.css";
// export default function Login() {
//   return (
//     <>
//       <div className="loginCardContainer">
//         <Card className="loginContainer" pt={"40px"}>
//           <CardBody>
//             <Stack spacing={5}>
//               <Input
//                 // value={value}
//                 // onChange={handleChange}
//                 placeholder="username"
//                 size="sm"
//               />

//               <Input
//                 // value={value}
//                 // onChange={handleChange}
//                 placeholder="password"
//                 size="sm"
//               />
//               <Center>
//                 <Button
//                   colorScheme="blue"
//                   size="xs"
//                   width={"50%"}
//                   alignItems={"center"}
//                 >
//                   Sign in
//                 </Button>
//               </Center>
//               <Center>
//                 <Button colorScheme="blue" size="xs" width={"50%"}>
//                   Sign up
//                 </Button>
//               </Center>
//             </Stack>
//           </CardBody>
//         </Card>
//       </div>
//     </>
//   );
// }
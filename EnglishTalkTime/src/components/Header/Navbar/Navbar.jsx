import { Grid, GridItem, Input, InputGroup, InputLeftElement } from "@chakra-ui/react"
import './Navbar.css'
import { NavLink } from "react-router-dom"

export default function Navbar({ user }) {


  return (
    <Grid templateColumns='repeat(5, 5fr)' margin='5' color='white' backgroundColor='#333333c4' padding='5' border='1px' borderColor='black' borderRadius='25px'>
      <GridItem ><NavLink to="/login">LogIn</NavLink></GridItem>
      {/* <GridItem ><NavLink to="/signup">SignUp</NavLink> </GridItem> */}
      <GridItem colSpan={2}>

      </GridItem>

    </Grid>
  )
}

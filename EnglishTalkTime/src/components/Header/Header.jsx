import React from "react";
import "./Header.css";
import Navbar from "./Navbar/Navbar";
import { useAuth } from "../../contexts/AuthContext";

import {
  Input,
  CloseButton,

} from "@chakra-ui/react";
import UserMenu from "./UserMenu/UserMenu";


const Header = () => {
  const { currentUser } = useAuth()


  return (
    <>
      <div className="header">

        <div className="header-logo">EnglishTalkTime</div>

        <div className="header-searchBar">
          <Input placeholder="Search..." />

          <CloseButton />
        </div>
        <div className="header-userInfo">
          
          {currentUser && <UserMenu />}
          {currentUser ? `Welcome, ${currentUser?.firstName} ` : <Navbar />}

          {/* <UserAvatar /> */}

        </div>
      </div>
    </>
  );
};

export default Header;

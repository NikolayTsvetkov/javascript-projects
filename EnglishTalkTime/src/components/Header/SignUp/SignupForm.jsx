import { FormControl, FormLabel, Card, Center, CardBody, Stack, Button, Input, Select } from '@chakra-ui/react';
import { useState } from 'react';
import { useAuth } from '../../../contexts/AuthContext';
import { registerUser } from '../../../services/auth.service';
import { useNavigate } from 'react-router-dom';
import "./SignupForm.css";
import { useValidation } from '../../../contexts/ValidationCOntext';



const inputFields = {
  username: '',
  password: '',
  firstName: '',
  lastName: '',
  email: ''
};

export default function SignUpForm() {
  const [fields, setFields] = useState(inputFields);
  const { username, password, firstName, lastName, email, level } = fields;
  const { handleSetCurrentUser } = useAuth();
  const navigate = useNavigate();
  const { errors, addError, clearErrors } = useValidation();

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFields({ ...fields, [name]: value });
  };

  const registerUsers = async (e) => {
    e.preventDefault();

    if (username.length < 5 || username.length > 35) {
      addError('Username should be between 5 and 35 symbols');
      return;
    }

    try {
      const user = await registerUser(fields);
      // console.log(user)
      handleSetCurrentUser(user);
      navigate('/');
    }
    catch (error) {
      console.log('SignUp error:', Object.keys(error), error.code, error.name, error.customData);

      addError(error.code)
    }
  };

  return (
    <>
      <Center h="100vh">
        <form onSubmit={registerUsers}>
          <h2 style={{ color: 'white' }}>Sign up here</h2>
          <Card className="loginContainer" bg="gray.800" pt={'40px'} color="white">
            <CardBody>

              <Stack spacing={5}>
                <FormControl isRequired>
                  <FormLabel>Username</FormLabel>
                  <Input
                    onChange={handleChange}
                    name="username"
                    value={username}
                    size="sm"
                    variant="filled"
                    bg="white"
                    color="black"
                    _hover={{ bg: 'gray.200' }}
                    _focus={{ bg: 'gray.200', border: '2px solid black', boxShadow: '0 0 0 1px black' }}
                  />
                </FormControl>

                <FormControl isRequired>
                  <FormLabel>Password</FormLabel>
                  <Input
                    onChange={handleChange}
                    name="password"
                    type={'password'}
                    value={password}
                    size="sm"
                    variant="filled"
                    bg="white"
                    color="black"
                    _hover={{ bg: 'gray.200' }}
                    _focus={{ bg: 'gray.200', border: '2px solid black', boxShadow: '0 0 0 1px black' }}
                  />
                </FormControl>

                <FormControl isRequired>
                  <FormLabel>First Name</FormLabel>
                  <Input
                    onChange={handleChange}
                    name="firstName"
                    value={firstName}
                    size="sm"
                    variant="filled"
                    bg="white"
                    color="black"
                    _hover={{ bg: 'gray.200' }}
                    _focus={{ bg: 'gray.200', border: '2px solid black', boxShadow: '0 0 0 1px black' }}
                  />
                </FormControl>

                <FormControl isRequired>
                  <FormLabel>Last Name</FormLabel>
                  <Input
                    onChange={handleChange}
                    name="lastName"
                    value={lastName}
                    size="sm"
                    variant="filled"
                    bg="white"
                    color="black"
                    _hover={{ bg: 'gray.200' }}
                    _focus={{ bg: 'gray.200', border: '2px solid black', boxShadow: '0 0 0 1px black' }}
                  />
                </FormControl>

                <FormControl isRequired>
                  <FormLabel>E-mail</FormLabel>
                  <Input
                    onChange={handleChange}
                    type="email"
                    name="email"
                    value={email}
                    size="sm"
                    variant="filled"
                    bg="white"
                    color="black"
                    _hover={{ bg: 'gray.200' }}
                    _focus={{ bg: 'gray.200', border: '2px solid black', boxShadow: '0 0 0 1px black' }}
                  />
                </FormControl>
                <Select
                  id="select"
                  color="black"
                  m={3}
                  name="level"
                  onChange={handleChange}
                  placeholder="Your English level"
                  variant="filled"
                  bg="white"
                  _hover={{ bg: 'gray.200' }}
                  _focus={{ bg: 'gray.200', border: '2px solid black', boxShadow: '0 0 0 1px black' }}
                >
                  <option value="A1">Beginner(A1)</option>
                  <option value="A2">Elementary(A2)</option>
                  <option value="B1">Intermediate(B1)</option>
                  <option value="B2">Upper-intermediate(B2)</option>
                  <option value="C1">Advanced(C1)</option>
                  <option value="C2">Mastery(C2)</option>
                </Select>


                <Center>
                  <Button
                    type="submit"
                    colorScheme="purple"
                    size="lg"
                    width="50%"
                    alignItems="center"
                    variant="solid"
                    bg="purple.500"
                    color="white"
                    _hover={{ bg: 'gray.500' }}
                    _active={{ bg: 'gray.600' }}
                  >
                    Sign up
                  </Button>

                </Center>
              </Stack>
            </CardBody>
          </Card>
        </form>
      </Center>
    </>
  );
}








// import { FormControl, FormLabel, Card, Center, CardBody, Stack, Button, Input } from '@chakra-ui/react'
// import { useState } from 'react'
// import { createUserWithEmailAndPassword } from 'firebase/auth'
// import "./SignupForm.css"
// // import { createUserObject } from '../../config/firebase-config'
// import { useAuth } from '../../../contexts/AuthContext'
// import { registerUser } from '../../../services/auth.service'


// const inputFields = {
//   username: '',
//   password: '',
//   firstName: '',
//   lastName: '',
//   email: ''
// }
// export default function SignUpForm() {

//   const [fields, setFields] = useState(inputFields)
//   const { username, password, firstName, lastName, email } = fields
//   console.log(fields)


//   const handleSubmit = async (e) => {
//     e.preventDefault()

//     try {

//       const { user } = await (createUserWithEmailAndPassword(email, password))

//       // history.push("/")
//     } catch (err) {
//       console.log("Something went wrong ", err)
//     }
//   }
//   const handleChange = (event) => {
//     const { name, value } = event.target
//     setFields({ ...fields, [name]: value })
//   }

//   const registerUsers = async (e) => {
//     e.preventDefault()
//     console.log(fields)
//     return await registerUser(fields)

//     // getUserByUsername(username)
//     //   .then((snapshot) => {

//     //     if (snapshot.exists()) {
//     //       throw new Error(`Username ${user.username} has already been taken!`);
//     //     }

//     //     return await.registerUser(fields?.email, fields?.password)
//     //   }).then(credential => {
//     //     createUserObjectByUserName(username, firstName, lastName, email, credential.uid)
//     //   }).then(setUser({
//     //     user: credential.user,
//     //   }))



//   }

//   return (
//     <>
//       <form onSubmit={registerUsers}>


//         <h2>Sign up here</h2>
//         <Card className="loginContainer" pt={'40px'}>
//           <CardBody>

//             <Stack spacing={5}>
//               <FormControl isRequired>
//                 <FormLabel>Username</FormLabel>
//                 <Input onChange={handleChange} name="username" value={username} />
//               </FormControl>

//               <FormControl isRequired>
//                 <FormLabel>Password</FormLabel>
//                 <Input onChange={handleChange} name="password" type={'password'} value={password} />
//               </FormControl>
//               <FormControl isRequired>
//                 <FormLabel>First Name</FormLabel>
//                 <Input onChange={handleChange} name="firstName" value={firstName} />
//               </FormControl>
//               <FormControl isRequired>
//                 <FormLabel>Last Name</FormLabel>
//                 <Input onChange={handleChange} name="lastName" value={lastName} />
//               </FormControl>
//               <FormControl isRequired>
//                 <FormLabel>E-mail</FormLabel>
//                 <Input onChange={handleChange} type={"email"} name="email" value={email} />
//               </FormControl>

//               <Center><Button type='submit' colorScheme='blue' size="xs" width={'50%'} alignItems={"center"}>Sign up</Button></Center>

//               <Button onClick={registerUsers}>show</Button>
//             </Stack>
//           </CardBody>
//         </Card>
//       </form>

//     </>



//   )
// }

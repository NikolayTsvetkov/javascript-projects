
import { Avatar } from "@chakra-ui/react";
import { useAuth } from "../../../contexts/AuthContext";

const AvatarBubble = () => {
  const context = useAuth();
//console.log(context?.currentUser?.photoURL)
  return (
    <>
      <Avatar src={context?.currentUser?.photoURL} />
    </>
  );
};

export default AvatarBubble;





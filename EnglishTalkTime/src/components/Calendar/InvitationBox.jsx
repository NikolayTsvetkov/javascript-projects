import React, { useRef, useState, useEffect } from 'react';
import { Box, Button, CloseButton, Input, Text } from '@chakra-ui/react';

const InvitationBox = ({ onClose, onSubmit }) => {
    const [invitationTitle, setInvitationTitle] = useState('');
    const [invitationEmail, setInvitationEmail] = useState('');
    const [isVideoCallActive, setIsVideoCallActive] = useState(false);
    const localVideoRef = useRef(null);
    const remoteVideoRef = useRef(null);
    const [localStream, setLocalStream] = useState(null);
    let peerConnection = null;

    useEffect(() => {
        if (localVideoRef.current && localStream) {
            localVideoRef.current.srcObject = localStream;
        }
    }, [localStream]);

    const handleInvitationSubmit = () => {
        onSubmit(invitationTitle, invitationEmail);
        setInvitationTitle('');
        setInvitationEmail('');
    };

    const startVideoCall = async () => {
        try {
            const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
            setLocalStream(stream);
            setIsVideoCallActive(true);
        } catch (error) {
            console.error('Error accessing media devices:', error);
        }
    };

    const handleVideoCall = () => {
        if (isVideoCallActive) {
            endVideoCall();
        } else {
            startVideoCall();
        }
    };

    const endVideoCall = () => {
        if (localStream) {
            localStream.getTracks().forEach((track) => track.stop());
        }
        if (localVideoRef.current) {
            localVideoRef.current.srcObject = null;
        }
        if (peerConnection) {
            peerConnection.close();
            peerConnection = null;
        }
        setIsVideoCallActive(false);
    };

    const createPeerConnection = () => {
        peerConnection = new RTCPeerConnection();
        peerConnection.addEventListener('icecandidate', handleIceCandidate);
        peerConnection.addEventListener('track', handleTrack);
        if (localStream) {
            localStream.getTracks().forEach((track) => peerConnection.addTrack(track, localStream));
        }
    };

    const handleIceCandidate = (event) => {
        if (event.candidate) {
            // Send the candidate to the other peer using your signaling mechanism
        }
    };

    const handleTrack = (event) => {
        if (remoteVideoRef.current) {
            remoteVideoRef.current.srcObject = event.streams[0];
        }
    };

    return (
        <Box
            bg="tertiary"
            p={4}
            rounded="md"
            width="25%"
            position="absolute"
            right={0}
            top={0}
            boxShadow="0 0 10px rgba(0, 0, 0, 0.2)"
            zIndex={1}
        >
            <CloseButton position="absolute" right={2} top={2} onClick={onClose} />
            <Text fontSize="lg" fontWeight="bold" mb={4}>
                Send Invitation
            </Text>
            <Input
                placeholder="Title"
                value={invitationTitle}
                onChange={(e) => setInvitationTitle(e.target.value)}
                mb={2}
            />
            <Input
                placeholder="Email"
                value={invitationEmail}
                onChange={(e) => setInvitationEmail(e.target.value)}
                mb={2}
            />
            <Button colorScheme="primary" onClick={handleInvitationSubmit} mb={2}>
                Send Invite
            </Button>
            <Button colorScheme="primary" onClick={handleVideoCall}>
                {isVideoCallActive ? 'End Video Call' : 'Start Video Call'}
            </Button>
            {isVideoCallActive && (
                <Box mt={4}>
                    <Text fontWeight="bold">Local Video:</Text>
                    <video ref={localVideoRef} autoPlay playsInline style={{ width: '100%', height: 'auto' }} />
                    <Text fontWeight="bold" mt={2}>
                        Remote Video:
                    </Text>
                    <video ref={remoteVideoRef} autoPlay playsInline style={{ width: '100%', height: 'auto' }} />
                </Box>
            )}
        </Box>
    );
};

export default InvitationBox;

import React, { useState } from 'react';
import { Box, Grid, Text, ChakraProvider, extendTheme, Button, Input } from '@chakra-ui/react';
import InvitationBox from './InvitationBox';


const theme = extendTheme({
    colors: {
        primary: '#6B46C1',
        secondary: '#000000',
        tertiary: '#D1D5DB',
        opposite: '#FFFFFF',
        textPalette: ['#CBA7E2', '#D4A8FF', '#E4ABFF', '#F1AEFF'],
    },
});

const Calendar = () => {

    const currentDate = new Date();

    const [displayedMonth, setDisplayedMonth] = useState(currentDate);

    const [showInvitationBox, setShowInvitationBox] = useState(false);

    const goToPreviousMonth = () => {
        setDisplayedMonth(new Date(displayedMonth.getFullYear(), displayedMonth.getMonth() - 1, 1));
    };

    const goToNextMonth = () => {
        setDisplayedMonth(new Date(displayedMonth.getFullYear(), displayedMonth.getMonth() + 1, 1));
    };

    const handleInvitationSubmit = (title, email) => {

        setShowInvitationBox(false);
    };


    const displayedMonthName = displayedMonth.toLocaleString('default', { month: 'long' });

    const totalDaysInMonth = new Date(displayedMonth.getFullYear(), displayedMonth.getMonth() + 1, 0).getDate();

    const firstDayOfMonth = new Date(displayedMonth.getFullYear(), displayedMonth.getMonth(), 1).getDay();

    const daysInMonth = Array.from({ length: totalDaysInMonth }, (_, index) => index + 1);

    return (
        <ChakraProvider theme={theme}>
            <Box
                bgGradient="linear(to-b, primary, tertiary)"
                minH="100vh"
                p={4}
                rounded="md"
                display="flex"
                flexDirection="column"
                alignItems="center"
            >
                <Box display="flex" alignItems="center" justifyContent="space-between" mb={4} width="100%">
                    <Button colorScheme="primary" onClick={goToPreviousMonth}>
                        Previous Month
                    </Button>
                    <Text color="primary" fontSize="xl" fontWeight="bold">
                        {displayedMonthName}
                    </Text>
                    <Button colorScheme="primary" onClick={goToNextMonth}>
                        Next Month
                    </Button>
                </Box>
                <Grid templateColumns="repeat(7, 1fr)" gap={2} width="100%">
                    {['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'].map((day) => (
                        <Text key={day} color="tertiary" textAlign="center" fontSize="sm">
                            {day}
                        </Text>
                    ))}
                    {Array(firstDayOfMonth)
                        .fill('')
                        .map((_, index) => (
                            <Text key={`empty-${index}`} />
                        ))}
                    {daysInMonth.map((day) => (
                        <Box
                            key={`day-${day}`}
                            bg={day === currentDate.getDate() && displayedMonth.getMonth() === currentDate.getMonth() ? 'primary' : 'tertiary'}
                            color={day === currentDate.getDate() && displayedMonth.getMonth() === currentDate.getMonth() ? 'opposite' : 'textPalette'}
                            textAlign="center"
                            fontSize={['sm', 'md', 'lg']}
                            fontWeight={day === currentDate.getDate() && displayedMonth.getMonth() === currentDate.getMonth() ? 'bold' : 'normal'}
                            p={2}
                            rounded="md"
                            height={`calc(100% - ${((100 / 7) * 0.07).toFixed(2)}%)`}
                            position="relative"
                            _hover={{
                                cursor: 'pointer',
                                opacity: 0.8,
                                transform: 'scale(1.05)',
                                boxShadow: '0 0 10px rgba(0, 0, 0, 0.2)',
                            }}
                            onClick={() => setShowInvitationBox(true)}
                        >
                            <Text
                                lineHeight="1.2"
                                textAlign="center"
                                fontFamily="cursive"
                                fontStyle="italic"
                            >
                                {day}
                            </Text>
                            <Text lineHeight="1.2" fontSize="sm">
                                {new Intl.DateTimeFormat('en-US', { weekday: 'long' }).format(new Date(displayedMonth.getFullYear(), displayedMonth.getMonth(), day))}
                            </Text>
                            <Text lineHeight="1.2" fontSize="sm">
                                Meet
                            </Text>
                            <Box
                                position="absolute"
                                top="50%"
                                left="50%"
                                transform="translate(-50%, -50%)"
                                opacity={0.4}
                                fontSize={['4xl', '5xl', '6xl']}
                                _hover={{
                                    opacity: 1,
                                }}
                                display="none"
                            >
                                +
                            </Box>
                            <Box
                                position="absolute"
                                top={0}
                                left={0}
                                width="100%"
                                height="100%"
                                display="flex"
                                alignItems="center"
                                justifyContent="center"
                                opacity={0}
                                _hover={{
                                    opacity: 1,
                                    background: 'rgba(255, 255, 255, 0.3)',
                                }}
                                transition="opacity 0.3s"
                            >
                                <Box
                                    fontSize={['4xl', '5xl', '6xl']}
                                    color="primary"
                                >
                                    +
                                </Box>
                            </Box>
                        </Box>
                    ))}
                </Grid>
                {showInvitationBox && (
                    <InvitationBox onClose={() => setShowInvitationBox(false)} onSubmit={handleInvitationSubmit} />
                )}
            </Box>
        </ChakraProvider>
    );
};


export default Calendar;

import { Button } from '@chakra-ui/button';
import { connect, createLocalTracks } from 'twilio-video';

function VideoCall() {

    const handleEnterRoom = () => {
        connect('7a1966e3dad98d4ba3a1b630564e7019', { name: 'my-new-room' }).then(room => {
            console.log(`Successfully joined a Room: ${room}`);
            room.on('participantConnected', participant => {
                console.log(`A remote Participant connected: ${participant}`);
            });
        }, error => {
            console.error(`Unable to connect to Room: ${error.message}`);
        });
    }


    return <div>
        Video Conf
        <Button onClick={handleEnterRoom}>Connect </Button>
    </div>
}
export default VideoCall
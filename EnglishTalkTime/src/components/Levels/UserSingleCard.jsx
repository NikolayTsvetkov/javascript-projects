import { Box, Grid, GridItem, Card, Image, Text, Button, HStack, VStack } from '@chakra-ui/react';
function UserSingleCard({ user }) {
    return <Card
        bg="gray.800"
        color="white"
        boxShadow="md"
        borderRadius="md"
        p={4}
        transition="transform 0.2s"
        _hover={{ transform: 'scale(1.05)' }}
        onClick={() => console.log('Card clicked')}
    >
        <HStack spacing={4}>
            <Image src={user.image} alt={user.name} borderRadius="md" boxSize={24} />

            <VStack align="flex-start" spacing={2} flex={1}>
                <Text fontSize="md" fontWeight="bold" color="white" textAlign="right">
                    {user.name}
                </Text>
                <Text fontSize="sm" fontWeight="bold" color="white" textAlign="right">
                    Age: {user.age}
                </Text>
                <Text fontSize="sm" fontWeight="bold" color="white" textAlign="right">
                    Country: {user.country}
                </Text>
                <Text fontSize="sm" fontWeight="bold" color="white" textAlign="right">
                    Level: {user.level}
                </Text>
            </VStack>
        </HStack>

        <Box my={4}>
            <Text>{user.about}</Text>
        </Box>
        <Button colorScheme="purple" size="sm">
            Send a Message
        </Button>
    </Card>
}

export default UserSingleCard
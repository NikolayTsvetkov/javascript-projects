import React from "react";
import "./LeftSidebarChild.css";
import { useEffect, useState } from "react";
import {
  LeftSidebarChildLoopDM,
  LeftSidebarChildLoop,
} from "./LeftSidebarChildLoops";
import { leftSidebarChildData } from "../../../testData/sideBarTestData";
import { fetchUsersByLevel } from "../../../services/level.services";
import { useAuth } from "../../../contexts/AuthContext";
import { LeftSidebarSearch } from "./LeftSidebarSearch/LeftSidebarSearch";
import {
  changesListenerUsersChat,
  createChat,
  fetchAllPMByUser,
} from "../../../services/chat.service";

const hardCodeUserUID = "ah7YaERd5jPwrJ0AzEUETiXKp4v2";
const participant1UID = "GGGGGGGGGGGGGGggggg";
const participant2UID = "vr1YhXAJbGMpSSNQRspNI38Ck7Q2";

const LeftSidebarChild = ({
  data,
  sendMessagesToParent,
  sendClickedItemToParent,
}) => {
  const {
    currentUser,
    handleSetUsersByLevel,
    currentUserFullInfo,
    userPersonalMessages,
  } = useAuth();
  const [messages, setMessages] = useState([]);
  const [clickedItemId, setClickedItemId] = useState(null);
  console.log('clickedItemId ', clickedItemId);
  const handleUsersByLevel = async (level) => {
    const usersByLevel = await fetchUsersByLevel(level);
    handleSetUsersByLevel(usersByLevel);
  };

  const handleUsersByPM = async (newMsg) => {
    setMessages( await fetchAllPMByUser(newMsg));
  };

  useEffect(() => {

    if (userPersonalMessages) {
      const setMessageCallback = (updatedMessages) => {
        handleUsersByPM(userPersonalMessages)
      };
      const unsubscribe = changesListenerUsersChat(setMessageCallback);
      return () => unsubscribe();
    }
  }, [userPersonalMessages]);

  useEffect(() => {
    if (messages) {
      sendMessagesToParent(messages);
    }
  }, [messages]);

  const handleChildData = () => {
    setMessages(messages);
  };

  const handleClickedItem = (item) => {
    sendClickedItemToParent(item);
    setClickedItemId(item);
  };

  const handleSubmitChat = () => {
    createChat(
      "Summary2!",
      "Hi just want to ask2?",
      participant1UID,
      participant1UID,
      participant2UID
    );
  };

  const handleSubmitChatByCurrentUser = () => {
    createChat(
      "Summary2 with currentUser!",
      "Ask11 about currentUser ?",
      currentUser?.uid,
      currentUser?.uid,
      participant2UID
    );
  };

  return (
    <div className="left-sidebar-child">
      <div className="left-sidebar-child">
        {data === "Personal Messages" && currentUser && (
          <div className="left-sidebar-child-conversation">
            <label>Messages</label>
            <LeftSidebarSearch />
            <button onClick={handleSubmitChat}>CREATE TEST CHAT</button>
            <button onClick={handleSubmitChatByCurrentUser}>
              CREATE CHAT currentUser
            </button>
            <div className="left-sidebar-child">
              {messages && messages.map((item, index) => (
                <LeftSidebarChildLoopDM
                  key={index}
                  item={item}
                  isActive={item.uid === clickedItemId?.uid}
                  currentUser={currentUser}
                  messages={messages}
                  sendMessagesToParent={handleChildData}
                  sendClickedItemToParent={handleClickedItem}
                />
              ))}
            </div>
          </div>
        )}

        {data === "Levels" && (
          <div>
            {" "}
            <label>Channels</label>
            <div className="left-sidebar-child-info">
              {leftSidebarChildData.map((item, index) => (
                <LeftSidebarChildLoop
                  handleClick={() => handleUsersByLevel(item?.level)}
                  key={index}
                  item={item}
                  isActive={item?.uid === clickedItemId?.uid}
                />
              ))}
            </div>
          </div>
        )}
      </div>
      <div className="left-sidebar-child-chats"></div>
    </div>
  );
};

export default LeftSidebarChild;

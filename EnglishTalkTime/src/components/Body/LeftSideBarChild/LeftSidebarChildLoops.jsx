import "./LeftSidebarChildLoops.css";
import { Avatar, Button, Flex, Icon, IconButton } from "@chakra-ui/react";
import textLengthChecker from "../../../checkFunctions/textLengthChecker";
import { getUserByUid } from "../../../services/users.service";
import { useEffect, useState } from "react";
import { findAnotherParticipant } from "../../../services/chat.service";
import { Messages } from "../Chat/Messages/Messages";

export const LeftSidebarChildLoop = ({ item, isActive, handleClick }) => {
  const classList = isActive
    ? "sidebar-child-loop active"
    : "sidebar-child-loop";

  return (
    <div>
      <Button
        key={item.id}
        bg="white"
        className={classList}
        variant="ghost"
        size="md"
        onClick={handleClick}
        _hover={{ backgroundColor: "gray.200" }}
        _focus={{ outline: "none" }}
      >
        {item.level && (
          <label className="sidebar-child-label">{item.level}</label>
        )}
      </Button>
    </div>
  );
};

export const LeftSidebarChildLoopDM = ({
  item,
  isActive,
  currentUser,
  sendMessagesToParent,
  sendClickedItemToParent,
}) => {
  const classList = isActive
    ? "sidebar-child-loop active"
    : "sidebar-child-loop";
  const [otherUserInfo, setOtherUserInfo] = useState(null);

  useEffect(() => {
    const waitingFunc = async () => {
      const responseUID = await findAnotherParticipant(item, currentUser);
      const responseUser = await getUserByUid(responseUID);
      setOtherUserInfo(responseUser);
    };
    waitingFunc();
  }, [item, currentUser]);
 
  const handleClick = (onClick) => {
    sendMessagesToParent(onClick);
    sendClickedItemToParent(item);
  };

  return (
    <button onClick={handleClick} className={classList}>
      <div className="btn-left-sidebar-child-loop">
        <div className="sidebar-child-icon">
          <Avatar
            name={otherUserInfo?.firstName}
            src={otherUserInfo?.photoURL}
          />
        </div>

        <div className="sidebar-child-theme-conversation">
          {item?.title && (
            <h2 className="sidebar-child-label">
              {textLengthChecker(item?.title)}
            </h2>
          )}
          {item?.content && (
            <p>
              <label className="sidebar-child-label-conversation">
                {textLengthChecker(item?.content)}
              </label>
            </p>
          )}
        </div>
      </div>
    </button>
  );
};

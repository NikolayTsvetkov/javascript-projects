import React, { useState } from "react";
import "./LeftSidebarSearch.css";
import { chatUsersSearch } from "../../../../services/users.service";

export const LeftSidebarSearch = () => {
  const [username, setUsername] = useState("");
  const [user, setUser] = useState(null);

  const handleKey = (e) => {
    if (e.code === "Enter") {
      console.log("Enter is entered");
      
      chatUsersSearch(username);
    }
    // e.code === "Enter" && chatUsersSearch(setUser, username);
  };

  return (
    <div className="search-form">
      <input
        className="search-form-input"
        type="text"
        placeholder="Find a user"
        onChange={(e) => setUsername(e.target.value)}
        onKeyDown={handleKey}
      />
    </div>
  );
};

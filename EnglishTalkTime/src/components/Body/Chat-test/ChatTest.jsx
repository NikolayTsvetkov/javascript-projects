import { useState, useEffect } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import { useAuth } from "../../../contexts/AuthContext";

export default function ChatTest() {
  const { currentUser } = useAuth;
  console.log('currentUser is: ',currentUser);
  const [inputContent, setInputContent] = useState("");
  const [users, setUsers] = useState([]);
  const [msg, setMsg] = useState([
    { head: "test1", ColumnHeading: "column test 1" },
    { head: "test2", ColumnHeading: "column test 2" },
    { head: "test3", ColumnHeading: "column test 3" },
  ]);

  const sendTestMsg = () => {
    console.log("inputContent  ", inputContent);
    setMsg([...msg, { head: "testche", ColumnHeading: inputContent }]);
    setInputContent("");
  };

  const showAllUsers = () => {
    return (
      <table className="table table-hover">
        {/* <br />
        <p>---USERS---</p> */}
        <tbody>
          {users.map((u) => (
            <tr key={u.uid}>
              <td>{u.fields.username}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  };

  const showAllMessages = () => {
    return (
      <table className="table table-hover">
        {/* <p>---Messages---</p> */}
        <tbody>
        {/* <th >---currentUser---</th> */}
        {/* <th >{currentUser}</th> */}
          {msg.map((el, i) => (
            <tr key={i}>
              <th scope="col">Type</th>
              <th scope="col">{el.head}</th>
              <th scope="col">{el.ColumnHeading}</th>
            </tr>
          ))}
        </tbody>
      </table>
    );
  };

  useEffect(() => {
    const db = getDatabase();
    const dbRef = ref(db, "users");
    const unsubscribe = onValue(dbRef, (snapshot) => {
      const usersData = [];
      snapshot.forEach((childSnapshot) => {
        const user = childSnapshot.val();
        console.log("user  ", user);
        usersData.push(user);
      });
      setUsers(usersData);
      console.log("users   ", usersData);
    });

    // Cleanup function to unsubscribe the listener when the component unmounts
    return () => {
      unsubscribe();
    };
  }, []);

  //   useEffect(() => {
  //     const db = getDatabase();

  //     const msgDidChanges = (uid) => {
  //       const dbRef = ref(db, `users/${uid}`);
  //       const unsubscribe = onValue(dbRef, (snapshot) => {
  //         const newMsgArr = [];

  //         snapshot.forEach((childSnapshot) => {
  //           const keyName = childSnapshot.key;
  //           console.log("keyName ", keyName);
  //           const data = childSnapshot.val();
  //           console.log("data  ", data);

  //           newMsgArr.push({ key: keyName, data: data });
  //         });

  //         setMsg((prevMsg) => [...prevMsg, ...newMsgArr]);
  //         console.log("msg  ", [...msg, ...newMsgArr]);
  //       });

  //       // Cleanup function to unsubscribe the listener when the component unmounts
  //       return () => {
  //         unsubscribe();
  //       };
  //     };
  //   }, []);

  return (
    <div>
      <p>chatTest</p>
      <br />

      <div className="form-group row">
        <label htmlFor="staticEmail" className="col-sm-2 col-form-label">
          Email
        </label>
        <div className="col-sm-10">
          <input
            style={{ backgroundColor: "yellow" }}
            type="text"
            className="form-control-plaintext"
            value={inputContent}
            onChange={(e) => setInputContent(e.target.value)}
            placeholder="Type here..."
          />
          <button onClick={sendTestMsg}>Send</button>
        </div>
      </div>
      {showAllMessages()}
      {showAllUsers()}
    </div>
  );
}

// import { set, ref, onValue} from "firebase/database";
// import { useState } from "react";

// export default function ChatTest() {
//   const [inputContent, setInputContent] = useState("");
//   const [users, setUsers] = useState([]);
//   const [msg, setMsg] = useState([
//     { head: "test1", ColumnHeading: "column test 1" },
//     { head: "test2", ColumnHeading: "column test 2" },
//     { head: "test3", ColumnHeading: "column test 3" },
//   ]);

//   const sendTestMsg = () => {
//     console.log("inputContent  ", inputContent);
//     setMsg([...msg, { head: "testche", ColumnHeading: inputContent }]);
//     setInputContent("");
//   };

//   const showAllUsers = () => {
//     return (
//       <div>
//         {users.map((u) => (
//           <p key={u.id}>{u.name}</p>
//         ))}
//       </div>
//     );
//   };

//   const usersDidChanges = () => {
//     const db = getDatabase();
//     const dbRef = ref(db, "users");
//     return onValue(dbRef, (snapshot) => {
//       const usersData = [];
//       snapshot.forEach((childSnapshot) => {
//         const user = childSnapshot.val();
//         console.log('user  ', user);
//         usersData.push(user);
//       });
//       setUsers([...usersData]);
//       console.log('users   ', users);
//     });
//   };

//   useEffect(() => {
//     usersDidChanges();
//   }, []);

//   const msgDidChanges = (uid) => {
//     const dbRef = ref(db, `users/${uid}`);
//     return onValue(dbRef, (snapshot) => {
//       let arr = [];

//       snapshot.forEach((childSnapshot) => {
//         let keyName = childSnapshot.key;
//         console.log('keyName ', keyName);
//         let data = childSnapshot.val();
//         console.log('data  ', data);

//         arr.push({ key: keyName, data: data });
//       });

//       setMsg([...msg, ...arr]);
//       console.log('msg  ', msg);
//     });
//   };

//   return (
//     <div>
//       <p>chatTest</p>
//       <br />

//       <div className="form-group row">
//         <label htmlFor="staticEmail" className="col-sm-2 col-form-label">
//           Email
//         </label>
//         <div className="col-sm-10">
//           <input
//             style={{ backgroundColor: "yellow" }}
//             type="text"
//             className="form-control-plaintext"
//             value={inputContent}
//             onChange={(e) => setInputContent(e.target.value)}
//             placeholder="Type here..."
//           />
//           <button onClick={() => sendTestMsg()}>Send</button>
//           <button onClick={() => showAllUsers()}>Show users</button>
//         </div>
//       </div>

//       <table className="table table-hover">
//         <tbody>
//           {msg.map((el, i) => {
//             return (
//               <tr key={i}>
//                 <th scope="col">Type</th>
//                 <th scope="col">{el.head}</th>
//                 <th scope="col">{el.ColumnHeading}</th>
//               </tr>
//             );
//           })}
//         </tbody>
//       </table>
//     </div>
//   );
// }

import { useState } from "react";
import "./LeftSidebarLoops.css";

export const LeftSidebarTopIcons = ({ item, isActive, handleClick }) => {
  const classList = `sidebar-loop ${ isActive ? "active" : ""}`;

  const handleClickInternal = () => {
    handleClick(item);
  };

  return (
    <button key={item.id} className={classList} onClick={handleClickInternal}>
      <div className="sidebar-icon">
      <i className={`${item.icon}`}></i>
      </div>
      {item.name && <label className="sidebar-label">{item.name}</label>}
    </button>
  );
};

export const LeftSidebarBottomIcons = ({ item, isActive, handleClick }) => {
  const classList = `sidebar-loop ${isActive ? "active" : ""}`;

  const handleClickInternal = () => {
    handleClick(item);
    console.log('click');
  };

  return (
    <button key={item.id} className={classList} onClick={handleClickInternal}>
      <div className="sidebar-icon-bottom">
        <item.icon />
      </div>
      {item.name && <label className="sidebar-label-bottom">{item.name}</label>}
    </button>
  );
};

import React from "react";
import "./ChatNavbar.css";

export const ChatNavbar = ({ currentUser, dataFromClickedLeftSidebarItem }) => {

  const showTopic = () => {
    if (dataFromClickedLeftSidebarItem.content) {
      return `Topic: ${dataFromClickedLeftSidebarItem.content}`
    } else {
      return null;
    }
  }
  return (
    <div className="chat-info">
      {/* <p>{currentUser && `${currentUser.firstName}` }</p> */}
      <p>{currentUser && dataFromClickedLeftSidebarItem && showTopic()}</p>
      <div className="chat-icons">
        <i className="fa-solid fa-video"></i>
        <i className="fa-solid fa-user-plus"></i>
        <i className="fa-solid fa-circle-info"></i>
      </div>
    </div>
  );
};

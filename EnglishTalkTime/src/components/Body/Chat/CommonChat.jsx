import React, { useState } from "react";
import { Box, Flex, Text, Input, Button } from "@chakra-ui/react";

const CommonChat = ({ users, currentUser }) => {
    const [message, setMessage] = useState("");
    const [chatMessages, setChatMessages] = useState([]);
    // console.log(users)
    const handleSendMessage = () => {
        // Handle sending the message
        const newMessage = {
            user: currentUser?.uid || 2,
            message,
        };

        setChatMessages([...chatMessages, newMessage]);
        setMessage("");
    };

    return (
        <Box bg="gray.800" color="white" borderRadius="md" p={4} width="100%">
            <Flex height="100%">
                <Box width="25%" pr={4}>
                    <Text fontWeight="bold" mb={2}>
                        Users
                    </Text>
                    {users.map((user) => (
                        <Text key={user?.uid} mb={2}>
                            {user?.fields?.firstName} {user?.fields?.lastName}
                        </Text>
                    ))}
                </Box>
                <Box width="75%">
                    <Box
                        height="75vh"
                        bgGradient="linear(to-b, purple.800, purple.500)"
                        borderRadius="md"
                        p={4}
                        overflowY="scroll"
                    >
                        {chatMessages.map((chat) => (
                            <Box
                                key={chat.user}
                                bgGradient={
                                    chat?.user === currentUser?.uid
                                        ? "linear(to-b, black, purple.800)"
                                        : "linear(to-b, purple.100, purple.500)"
                                }
                                borderRadius="md"
                                p={2}
                                mb={2}
                            >
                                <Text color={chat?.user === currentUser?.uid ? "white" : "black"}>
                                    {chat?.message}
                                </Text>
                            </Box>
                        ))}
                    </Box>
                    <Flex mt={4}>
                        <Input
                            value={message}
                            onChange={(e) => setMessage(e.target.value)}
                            placeholder="Enter your message..."
                            flex="1"
                            mr={2}
                        />
                        <Button onClick={handleSendMessage} colorScheme="purple">
                            Send
                        </Button>
                    </Flex>
                </Box>
            </Flex>
        </Box>
    );
};

export default CommonChat;

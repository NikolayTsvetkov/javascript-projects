import React, { useState } from "react";
import "./Input.css";
import {
  addConversationMessage,
  formatTimestamp,
} from "../../../../services/chat.service";

export default function Input({
  dataFromClickedLeftSidebarItem,
  currentUser
}) {
  const [inputValue, setInputValue] = useState("");

  const handleChange = (event) => {
    setInputValue(event.target.value);
  };

  const handleClearInput = () => {
    setInputValue("");
  };
 
  const handleClick = () => {
    const obj = {
      author: currentUser.uid,
      content: inputValue,
      createdOn: formatTimestamp(Date.now()),
      uid: dataFromClickedLeftSidebarItem.uid,
    };
    
    addConversationMessage(dataFromClickedLeftSidebarItem.uid, obj);
    handleClearInput();
  };

  return (
    <div className="messages-input">
      <input
        className="input-field"
        type="text"
        placeholder="Type something..."
        value={inputValue}
        onChange={handleChange}
      />
      <div className="send">
        {inputValue && (
          <div>
            <button className="clear-button" onClick={handleClearInput}>
              <i className="fa-solid fa-xmark"></i>
            </button>
          </div>
        )}{" "}
        <div className="send">
          <i className="fa-solid fa-paperclip"></i>
          <i className="fa-solid fa-paper-plane" onClick={handleClick}></i>
        </div>
      </div>
    </div>
  );
}

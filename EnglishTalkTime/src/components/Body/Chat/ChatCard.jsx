import { Box, Button, Text, Image, Badge, Circle } from "@chakra-ui/react";

const ChatCard = ({ user, currentUser }) => {
    const { firstName, lastName, level, uid } = user.fields;
    const status = currentUser?.uid === uid
    // Placeholder image URL
    //TODO: Invite and Messages send invitation for chat from currentUser to choose user
    const imageUrl = user.imageUrl || "https://www.gravatar.com/avatar/?d=mp&s=150";

    return (
        <Box
            borderWidth="1px"
            borderRadius="lg"
            p={4}
            bg="gray.700"
            color="white"
            _hover={{ bg: "gray.600" }}
            transition="background-color 0.2s ease"
            textAlign="center"
            display="flex"
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            width="250px"
            height="300px"
            key={uid}
        >
            <Box mb={4}>
                <Image
                    src={imageUrl}
                    alt={`${firstName} ${lastName}`}
                    borderRadius="full"
                    boxSize="150px"
                    objectFit="cover"
                    fallbackSrc="https://via.placeholder.com/150"
                />
            </Box>
            <Text fontSize="lg" fontWeight="bold">
                {`${firstName} ${lastName}`}
            </Text>
            <Circle
                size="10px"
                bg={status === "online" ? "green.500" : "red.500"}
            />
            <Text>{level}</Text>
            <Box display="flex" justifyContent="space-between" mt={4}>
                <Button
                    colorScheme="purple"
                    size="sm"
                    flex="1"
                    mr={2}
                    _focus={{ boxShadow: "none" }}
                >
                    Invite
                </Button>
                <Button
                    colorScheme="purple"
                    size="sm"
                    flex="1"
                    ml={2}
                    _focus={{ boxShadow: "none" }}
                >
                    Message
                </Button>
            </Box>
        </Box>
    );
};

export default ChatCard
import React, { useEffect, useState } from "react";
import { Message } from "./Message";
import "./Messages.css";

export const Messages = ({
  dataFromClickedLeftSidebarItem,
  dataFromLeftSidebarChild,
  currentUser
}) => {
  const [chat, setChat] = useState([]);
  const selectedChatUid = dataFromClickedLeftSidebarItem.uid;
  const userChats = dataFromLeftSidebarChild;
  const clickedItem = userChats.find((item) => item.uid === selectedChatUid);
  const selectedChat = clickedItem && clickedItem.conversation;

  useEffect(() => {
    selectedChat ? setChat(selectedChat) : setChat([]);
  }, [dataFromLeftSidebarChild, dataFromClickedLeftSidebarItem]);

  const handleMessageClick = () => {
    console.log("click handleMessageClick");
  };  

  return (
    <div>
      {currentUser && chat &&
        chat.map((message, index) => (
          <div key={index}>
            <Message
              isOwner={message?.author === currentUser?.uid}
              value={message}
              onClick={handleMessageClick}
              currentUser={currentUser}
              selectedChat={selectedChat}
            />
          </div>
        ))}
    </div>
  );
};

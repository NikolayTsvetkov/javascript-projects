import React, { useEffect, useState } from "react";
import { Avatar } from "@chakra-ui/react";
import "./Message.css";
import { getUserPhotoURL } from "../../../../services/users.service";

export const Message = ({ isOwner, value }) => {
  const [showButtons, setShowButtons] = useState(false);
  const [authorPhotoURL, setAuthorPhotoURL] = useState("");
  const messageClass = !isOwner ? "message-owner" : "message";
  const messageColor = isOwner ? "#272849" : "#801414";

  useEffect(() => {
    const fetchAuthorPhotoURL = async () => {
      setAuthorPhotoURL(await getUserPhotoURL(value?.author));
    };

    fetchAuthorPhotoURL();
  }, [value?.author]);

  const handleOnClick = () => {
    setShowButtons((prevShowButtons) => !prevShowButtons);
  };

  const handleDeleteClick = async () => {
    console.log("Delete message:", value);
    console.log('index ', value.index);
  };

  const handleEditClick = () => {
    console.log("Edit message:", value);
  };

  return (
    value && (
      <div className={messageClass} onClick={handleOnClick}>
        <div className="message-info">
          <Avatar name={value?.author} src={authorPhotoURL}/>
          <div className="time-span">
            <b><span>{value?.createdOn}</span></b>
          </div>
        </div>

        <div
          className="message-content"
          style={{ backgroundColor: messageColor }}
        >
          <p>{value?.content}</p>
        </div>

        {showButtons && (
          <div className="message-buttons">
            <button className="delete-button" onClick={handleDeleteClick}>
              delete
            </button>
            <button className="edit-button" onClick={handleEditClick}>
              edit
            </button>
          </div>
        )}
      </div>
    )
  );
};

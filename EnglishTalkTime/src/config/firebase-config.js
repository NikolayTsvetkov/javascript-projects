import { initializeApp } from "firebase/app";
import { getDatabase } from 'firebase/database'
import { getAuth } from "firebase/auth";
import { getStorage, ref} from "firebase/storage";


const firebaseConfig = {
    apiKey: "AIzaSyD8nba1IjsfWacgi3Xii7nVb0Mf0XazA78",
    authDomain: "chat-project-fc23c.firebaseapp.com",
    projectId: "chat-project-fc23c",
    storageBucket: "chat-project-fc23c.appspot.com",
    messagingSenderId: "844601050846",
    appId: "1:844601050846:web:8d33ec3dfb7e3ce194a44f",
    databaseURL: 'https://chat-project-fc23c-default-rtdb.europe-west1.firebasedatabase.app/'
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)
export const db = getDatabase(app)
export const storage = getStorage(app);
export const storageRef = ref(storage);
export const profilePicturesRef = ref(storage, '/profilePictures')













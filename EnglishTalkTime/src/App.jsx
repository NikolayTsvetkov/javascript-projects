import ValidationModal from "./components/modals/ValidationModal";
import "./App.css";
import UsersGridByLevel from "./components/Levels/UsersGridByLevel";
import Body from "./components/Body/Body";
import ChatTest from "./components/Body/Chat-test/ChatTest";
//import Header from "./components/Header/Header";
import Landing from "./components/Landing/Landing";
import Login from "./components/Header/Login/Login";
import SignUpForm from "./components/Header/SignUp/SignupForm";
import { Route, Routes, NavLink } from "react-router-dom";
import Chat from "./components/Body/Chat/Chat";
import LeftSidebarChild from "./components/Body/LeftSideBarChild/LeftSidebarChild";
//import VideoCall from "./components/Video/VideoCall";

const App = () => {
  return (
    <div className="appContainer">
      <ValidationModal />

      {/* <Header /> */}
      <Routes>
        <Route path="/" element={<Landing />} />
        <Route path="/signup" element={<SignUpForm />} />
        <Route path="/login" element={<Login />} />
        <Route path="/profile" element={<UsersGridByLevel />} />
        <Route path="/chat/levels" element={<LeftSidebarChild />} />

        {/* <Route path="/chat" element={<ChatTest />} /> */}
        {/* <Route path="/video" element={<VideoCall />} /> */}
      </Routes>
      <Body />
    </div>
  );
};

export default App;

import { get, onValue, ref, push, query, equalTo, orderByChild, update, set, remove, startAt, endAt, onChildAdded } from "firebase/database";
import { db } from "../config/firebase-config";
import { getDatabase } from "firebase/database";
import { getUserByUid } from "./users.service";

export const fromMessagesDocument = (snapshot) => {
  if (!snapshot.exists()) return [];

  const messagesDocument = snapshot.val();
  return Object.keys(messagesDocument).map((key) => {
    const message = messagesDocument[key];

    return {
      ...message,
      id: key,
      createdOn: new Date(message.createdOn),
      likedBy: message.likedBy ? Object.keys(message.likedBy) : [],
    };
  });
};

export const addMessage = async (content, handle) => {
  const result = await push(
    ref(db, 'messages'),
    {
      content,
      author: handle,
      createdOn: Date.now(),
    }
  );
  return getPostById(result.key);
};

const addMessageIdToUser = async (handle, messageId, participant2) => {
  const updateComments = {};
  updateComments[`/users/${handle}/messages/${messageId}`] = true;
  updateComments[`/users/${participant2}/messages/${messageId}`] = true;
  return update(ref(db), updateComments);
};

const addIdToMessage = async (messageId) => {
  const updateComments = {};
  updateComments[`/usersChat/${messageId}/uid`] = messageId;
  return update(ref(db), updateComments);
};

export const addConversationMessage = async (uid, message) => {
  const conversationRef = ref(db, `usersChat/${uid}/conversation`);
  const snapshot = await get(conversationRef);
  let conversation = snapshot.val() || [];
  conversation.push(message);
  return set(conversationRef, conversation);
};

export const createChat = async (title, content, handle, participant1, participant2) => {
  const chatRef = ref(db, 'usersChat');
  const chatData = {
    title,
    content,
    participants: [participant1, participant2],
    author: handle,
    createdOn: Date.now(),
  };

  try {
    const chatSnapshot = await push(chatRef, chatData);
    const messageId = chatSnapshot.key;
    console.log('messageId  ', messageId);
    await addMessageIdToUser(handle, messageId, participant2);
    await addIdToMessage(messageId);
    return chatSnapshot;
  } catch (error) {
    console.error("Error creating chat: ", error);
  }
};

export const getAllMessagesByUserUID = async (handleUID) => {
  const snapshot = await get(ref(db, `users/${handleUID}`));

  if (!snapshot.exists()) return [];
  return Object.keys(snapshot.val().messages);
};

export const getMessagesByHandle = async (handle) => {
  const snapshot = await get(ref(db, `usersChat/${handle}`));
  console.log('snapshot ', snapshot.val());
  if (!snapshot.exists()) throw new Error(`User with id '${handle}' does not exist!`);
  return snapshot.val();
};

export const getLiveMessages = (chatId, listen) => {
  return onValue(ref(db, `channels/${chatId}/messages`), listen);
};
export const getLiveMessagesById = (handle) => {
  const db = getDatabase();
  const dbRef = ref(db, `usersChat/${handle}/conversation`);

  console.log('getLiveMessagesById');
  return onValue(dbRef, (snapshot) => {
    console.log('getLiveMessagesById inside onValue');
    if (!snapshot.exists()) return [];
    return snapshot.val();
  });
};

export const changesListenerUsersChat = (setMessages) => {
  const db = getDatabase();
  const dbRef = ref(db, "usersChat");

  const callback = (snapshot) => {
    const usersChat = [];
    snapshot.forEach((childSnapshot) => {
      const uid = childSnapshot.key;
      const msg = {
        uid,
        ...childSnapshot.val()
      };
      usersChat.push(msg);
    });
    setMessages(usersChat);
  };

  const unsubscribe = onValue(dbRef, callback);

  return () => unsubscribe();
};

export const changesListenerConversation = (uid, setConversation) => {
  console.log("inside of changesListenerConversation");
  const db = getDatabase();
  console.log('db ', db);
  const dbRef = ref(db, `userChats/${uid}/conversation`);
  console.log('dbRef  ', dbRef);
  const unsubscribe = onChildAdded(dbRef, (snapshot) => {
    console.log('snapshot.val()-changesListenerConversation ', snapshot.val());
    console.log('snapshot-changesListenerConversation ', snapshot);
    const message = snapshot.val();
    setConversation((prevConversation) => [...prevConversation, message]);
  });

  return () => unsubscribe();
};

export const findMessageAuthor = async (msg) => {
  const response = await getUserByUid(msg.author);
  console.log("response  ", response);
  return response;
};

export const findAnotherParticipant = async (msg, currentUser) => {
  return await currentUser.uid === msg.author
    ? msg.participants.find(participant => participant !== currentUser.uid)
    : msg.author;
}

export const msgDidChanges = (uid) => {
  const dbRef = ref(db, `users/messages/${uid}`);
  return onValue(dbRef, (snapshot) => {
    let record = []
    snapshot.forEach(childSnapshot => {
      let keyName = childSnapshot.key
      let data = childSnapshot.val();
      record.push({ "key": keyName, "data": data })
    })
  });
};

export const formatTimestamp = (timestamp) => {
  const formattedDate = new Date(timestamp).toLocaleDateString("en-GB", {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
  });

  const formattedTime = new Date(timestamp).toLocaleTimeString("en-US", {
    hour12: false,
    hour: "2-digit",
    minute: "2-digit",
  });

  const [time, period] = formattedTime.split(" ");
  const formattedTimestamp = `${time}, ${formattedDate}`;

  return formattedTimestamp;
};


export const fetchAllPMByUser = async (handle) => {
  if (Array.isArray(handle)) {
    const promises = handle.map(async (item) => {
      const snapshot = await get(ref(db, `usersChat/${item}`));
      if (!snapshot.exists()) throw new Error(`usersChat with id '${item}' does not exist!`);
      
      return snapshot.val();
    });
    const results = await Promise.all(promises);
  
    return results;
  }
 
};




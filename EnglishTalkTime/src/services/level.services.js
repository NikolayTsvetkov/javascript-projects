import { getUsersByLevel } from "./users.service";

export const fetchUsersByLevel = async (level) => {
    console.log(level);

    try {
        const usersByLevel = await getUsersByLevel(level);
        console.log(usersByLevel);

        return usersByLevel
    } catch (err) {
        console.log(err);

    }
}
import React, { createContext, useContext, useEffect, useState } from "react";
import { auth } from "../config/firebase-config";
import { getUserByUid, getUserDB, getUserPersonalMessagesUid } from "../services/users.service";

const AuthContext = createContext();

export function useAuth() {
    return useContext(AuthContext);
}

export function AuthProvider({ children }) {
    const [currentUser, setCurrentUser] = useState(() => {
        const storedUser = localStorage.getItem("currentUser");
        return storedUser ? JSON.parse(storedUser) : null;
    });
    const [currentUserFullInfo, setCurrentUserFullInfo] = useState({});
    const [usersByLevel, setUsersByLevel] = useState([]);
    const [userPersonalMessages, setUserPersonalMessages] = useState({});

    useEffect(() => {
        if (currentUser) {
            // localStorage.setItem('userCredential', '');
            // localStorage.setItem('currentIser', JSON.stringify(currentUser));
            fetchUserFullInfo(currentUser);
            fetchUserPersonalMessages(currentUser)
        }
    }, [currentUser]);

    const signup = (email, password) => {
        return auth.createUserWithEmailAndPassword(email, password);
    };

    const login = (email, password) => {
        return auth.signInWithEmailAndPassword(email, password);
    };

    const logout = () => {
        auth.signOut();
        setCurrentUser(null);
        localStorage.removeItem("currentUser");
    };

    const handleSetCurrentUser = async (user) => {
        const userDb = await getUserByUid(user.uid);
        const currentUserData = { ...user, ...userDb };
        setCurrentUser(currentUserData);
        localStorage.setItem("currentUser", JSON.stringify(currentUserData));
    };

    const handleSetUsersByLevel = async (users) => setUsersByLevel(users);

    const fetchUserFullInfo = async (user) => {
        const result = await getUserByUid(user.uid);
        setCurrentUserFullInfo(result);
    };

    const fetchUserPersonalMessages = async (user) => {
        const result = await getUserPersonalMessagesUid(user.uid);
        setUserPersonalMessages(result);
    };

    const value = {
        currentUser,
        currentUserFullInfo,
        usersByLevel,
        userPersonalMessages,
        login,
        signup,
        logout,
        handleSetCurrentUser,
        handleSetUsersByLevel,
    };

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

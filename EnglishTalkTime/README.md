# English Talk TIme

## Tech Stack
<ul>
    <li>React</li>
    <li>Firebase</li>
    <li>RTCPeerConnection</li>
    <li>ChakraUI</li>
</ul>

## Description
Built with React and WebRTC, English Talk Time offers a seamless and real-time communication experience. To To enhance the user experience, the project leverages Chakra UI.
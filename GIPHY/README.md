# GIPHY Project

## Description
<p>The application displays trending giphies from <a href="http://giphy.com">giphy.com</a> on its home page. You can check out each giphy and like it. The liked ones are saved in FAVORITES. If your favorites list is empty, the app will provide a random giphy for you. You can upload giphies and display them in MY UPLOADS section.</p>

<p>The app displays trending giphies on its home page. You can check out each giphy and like it. There is a section for the liked ones. If your favorites list is empty, the app will provide a random giphy for you. You can upload giphies and display them in MY UPLOADS section.</p>

## Technologies

<ul>
  <li>JavaScript</li>
  <li>React</li>
  <li>ChakraUI</li>
  <li>Firebase</li>
</ul>




import { getRandomGiphy } from '../requests/queries.js';
import renderFavourites from '../views/favourites.js';
import getFavouritiesGiphys from './getFavouriteGiphts.js';

/**
 * showFavouritesState It gets the favourites from the local storage
 * and in case of favourites present, shows the appropriate view
 * @author Nikolay Tsvetkov <nikolay.val.tsvetkov@gmail.com>
 * @param {Array} favs empty array or array with favourite giphys got by the local storage
 *  @param {HTMLElement} giphyContainer The main container that renders different views
 *  @returns {HTMLElement} The view for favourites
 */
const showFavouritesState = async (favs, giphyContainer) => {
  if (favs && favs.length > 0) {
    const favGiphys = await getFavouritiesGiphys(favs);
    giphyContainer.innerHTML = renderFavourites(favGiphys);
  } else {
    const giphy = await getRandomGiphy();
    giphyContainer.innerHTML = renderFavourites([giphy.data], true);
  }
};
export default showFavouritesState;

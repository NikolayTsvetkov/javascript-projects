import { getGiphyById } from '../requests/queries.js';
/**
 * getFavouritiesGiphys Function that accepts favourites array and for eash giphy
 * does a query to get its details.
 * @author Nikolay Tsvetkov <nikolay.val.tsvetkov@gmail.com>
 * @param {Array} favs empty array or array with favourite giphys
 * @returns {Array} Array with all giphies data fetched from the api
 */
const getFavouritiesGiphys = async (favs) => {
  const data = [];
  for (let i = 0; i < favs.length; i += 1) {
    const giphyId = favs[i];
    const favGiphy = await getGiphyById(giphyId);
    data.push(favGiphy.data);
  }

  return data;
};

export default getFavouritiesGiphys;

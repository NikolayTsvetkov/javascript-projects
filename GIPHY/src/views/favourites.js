/**
 * renderFavourites Function that renders the html for favourite giphies
 * It shows a random giphy if none is picked otherwise shows the favourite ones
 * which are saved in the localstorage.
 * @author Nikolay Tsvetkov <nikolay.val.tsvetkov@gmail.com>
 * @param {Array} favs empty array or array with favourite giphys
 * @param {boolean} isRandom if the component has to render the random view
 * @returns {HTMLElement} The view for favourites
 */
const renderFavourites = (favs = [], isRandom = false) => {
  if (!isRandom) {
    return `
         
          <div class='favourites-container'>
          <h1 class='fav-title'>Your favourite giphys</h1>
          <dic class='inner-favourites-container'>
            ${favs.map((giphy) => `
            <div class='inner-content-div-test'>
                <div class='image-wrapper'>
                        <img src=${giphy.images['480w_still'].url}' />
                        <div>     
                            <span class='fav'>
                                <i class='fa fa-heart' style='color:red;' data-heart-icon=${giphy.id}></i>
                            </span>
                            <p class='inner-content-div-p view-movie-btn' data-giphy-id='${giphy.id}'>View details</p>
                        </div>
                </div>
            </div>
            `)}
            </div>
          </div>
          `;
  }
  return `
      <div class='favourites-container-random'>
          <h1 class='fav-title'>Seems we have to pick for you!</h1>
          <h6 class='inner-fav-title'>You can like it!</h6>
          <div class='inner-content-div-test'>
            <div class='image-wrapper'>
                    <img src=${favs[0].images['480w_still'].url} height='200px' width='240px' />
                    <div>     
                        <span class='fav'>
                            <i class='fa fa-heart' style='color:white;' data-heart-icon=${favs[0].id}></i>
                        </span>
                        <p class='inner-content-div-p view-movie-btn' data-giphy-id='${favs[0].id}'>View details</p>
                    </div>
            </div>
          </div>
      </div>
  
      `;
};

export default renderFavourites;

import { searchGiphy } from '../requests/queries.js';
import renderTrendingView from './trending-gifs.js';

/**
 * clearBtnAppend - Appends a clear button to the search container element
 * and attaches a click event listener to it.
 * @author Georgi Tsekleov <jorkata99@abv.bg>
 * @returns {void}
 */
export const clearBtnAppend = () => {
  const searchContainer = document.querySelector('.search-container');
  let clearButton = searchContainer.querySelector('.clear-button');

  if (!clearButton) {
    clearButton = document.createElement('button');
    clearButton.classList.add('clear-button');
    clearButton.innerText = 'Clear';
    searchContainer.appendChild(clearButton);

    clearButton.addEventListener('click', () => {
      clearButton.style.display = 'none';
      const res = document.getElementById('search').value = '';
      searchShowHide(res.length);
    });
  } else {
    clearButton.style.display = 'block';
  }
};

/**
 * deleteBtnAppend - Hides the clear button if it's currently visible.
 * @author Georgi Tsekleov <jorkata99@abv.bg>
 * @returns {void}
 */
export const deleteBtnAppend = () => {
  if (document.querySelector('.clear-button').style.display = 'block') {
    document.querySelector('.clear-button').style.display = 'none';
  }
};

/**
 * searchShowHide - Shows or hides the current result
 * element based on the length of the search input.
 * @param {number} value - The length of the search input.
 * @returns {void}
 */
export const searchShowHide = (value) => {
  const x = document.getElementById('current-result');
  if (value > 1) {
    x.style.display = 'flex';
  } else {
    x.style.display = 'none';
    document.getElementById('search').value = '';
    deleteBtnAppend();
  }
};

/**
 * searchFunction - Renders result data from search input.
 * @author Georgi Tsekleov <jorkata99@abv.bg>
 * @param {string} input - The text input from the search field.
 * @returns {HTMLElement} The container element for the search results.
 */
export const searchFunction = async (event, trendingGifs, giphyContainer) => {
  const currentValue = event.target.value;

  if (event.target.id === 'search' && currentValue.length > 1) {
    const searchResults = await searchGiphy(currentValue);
    document.getElementById('interactive-display-inner-span').innerHTML = currentValue;

    clearBtnAppend();
    searchShowHide(currentValue.length);
    return renderTrendingView(searchResults, giphyContainer);
  }

  if (currentValue.length < 1) {
    clearBtnAppend();
    searchShowHide(currentValue.length);
    return renderTrendingView(trendingGifs, giphyContainer);
  }
};

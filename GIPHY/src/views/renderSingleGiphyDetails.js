/**
* renderSingleGiphyDetails - Generates an HTML string for the details
* of a single Giphy in the About view.
 * @author Nikolay Tsvetkov <nikolay.val.tsvetkov@gmail.com>
 * @param {Object} giphy - The Giphy object to be rendered.
* @returns {HTMLElement} The HTML string representing the details of a single Giphy
    in the About view.
*/

const renderSingleGiphyDetails = (giphy) => `
<div class='render-single-details'>
        <img  src=${giphy.images['downsized_medium'].url} />
        <hr>
        <div class="inner-text-btn-details">
            <i><b><p>${giphy.title ? `Title: ${giphy.title}` : ''}</p></b></i>
            <p>${giphy.username ? `Username: ${giphy.username}` : ''}</p>
            <p>${giphy.rating ? `Rating: ${giphy.rating.toUpperCase()}` : ''}</p>
            <p>${giphy.import_datetime ? `Impored: ${giphy.import_datetime}` : ''}</p>
        </div>

</div>`;

export default renderSingleGiphyDetails;

/**
 * toAboutView - Generates an HTML string for the About view.
 * @author Georgi Tsekleov <jorkata99@abv.bg>
 * @returns {string} The HTML string representing the About view.
 */
const toAboutView = () => `
<div class="fav-title">
  <div class="about-view">
    <h2>About the app</h2>
    <p>Authors: Telerik Academy group</p>
    <p>Team: 10</p>
    <p>${new Date().toDateString()}</p>
  </div>
</div>
`;
export default toAboutView;

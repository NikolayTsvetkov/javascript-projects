/**
 * renderFavourite Function that renders the html for hearts
 * It handles the two states of the heart - red and white
 * red for when the giphy is liked
 * @author Nikolay Tsvetkov <nikolay.val.tsvetkov@gmail.com>
 * @param {boolean} fav value if the giphy is in favourites in the localstorage
 * @param {string} id the id of the giphy
 * @returns {HTMLElement} The view of the heart
 */
const renderFavourite = (fav, id) => {
  if (fav) {
    return `<i class='fa fa-heart' style="color:red;" data-heart-icon=${id}></i>`;
  }
  return `<i class='fa fa-heart' style="color:white;" data-heart-icon=${id}></i>`;
};
export default renderFavourite;

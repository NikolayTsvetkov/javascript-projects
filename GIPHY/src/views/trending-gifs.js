import renderSingleGiphy from './single-giphy.js';

/**
 * Gets an object which coontains an array of GIFS(.data)and paste it at a certain place in the HTML
 * @author Radoslav Dimitrov <radosl.dimitrov@gmail.com>
 * @param {Object} trendyGiphys
 * @param {string} giphyContainer
 * @returns
 */
const renderTrendingView = (trendyGiphys, giphyContainer) => {
  const giphyList = trendyGiphys.data
    .map((giphy) => renderSingleGiphy(giphy))
    .join('');
  giphyContainer.innerHTML = giphyList;
};

export default renderTrendingView;

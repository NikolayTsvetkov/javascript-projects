import renderFavourite from './renderFavourite.js';
/**
 ** renderSingleGiphy- Generates an HTML string for the single giphy
* @author Nikolay Tsvetkov <nikolay.val.tsvetkov@gmail.com>
* @param {Object} giphy - The Giphy object to be rendered.
* @returns {HTMLElement} The HTML string representing for the single giphy
*/
const renderSingleGiphy = (giphy) => `
<div class='inner-content-div-test'>
  <div class='image-wrapper'>

      <img class='still-img' src=${giphy.images['480w_still'].url} />
      <img class='gif-img' src=${giphy.images.original.url} />
      
      <p class='inner-content-div-p view-movie-btn' data-giphy-id='${giphy.id}'>View details</p>
      <span class='fav top-right'>${renderFavourite(giphy.fav, giphy.id)}</span>
    </div>
</div>
`;

export default renderSingleGiphy;

import {
  API_KEY, FAVORITES, MY_UPLOADS, USERNAME,
} from '../common/constants.js';

/**
 * getGiphyById Fetch data from API, a single giphy by given id
 * @author Nikolay Tsvetkov <nikolay.val.tsvetkov@gmail.com>
 * @param {string} id
 * @returns {Promise}  Object with data about the giphy
 */
export const getGiphyById = async (id) => {
  const query = await fetch(`https://api.giphy.com/v1/gifs/${id}?api_key=${API_KEY}`);

  const data = await query.json();
  return data;
};
/**
* getTrendingGifs - fetches data from API, 20 trending GIFs
* @author Radoslav Dimitrov <radosl.dimitrov@gmail.com>
* @returns {object} Object whose data property holds an array of GIFs.
*/
export const getTrendingGifs = async () => {
  const trendingGifsRaw = await fetch(`https://api.giphy.com/v1/gifs/trending?api_key=${API_KEY}&limit=20&rating=g%27`);
  const trendingGifs = await trendingGifsRaw.json();
  let favGiphys = [];
  if (window.localStorage.getItem(FAVORITES)) {
    favGiphys = await JSON.parse(window.localStorage.getItem(FAVORITES));
  }
  if (favGiphys.length > 0) {
    trendingGifs.data.map((giphy) => {
      if (favGiphys.some((giphyId) => giphyId === giphy.id)) {
        giphy.fav = true;
        return giphy;
      }
    });
  }
  return trendingGifs;
};
/**
* uploadGif - uploads a GIF selected from the file system.
* @author Radoslav Dimitrov <radosl.dimitrov@gmail.com>
* @returns {Promise}
*/
export const uploadGif = async (gifToUpload) => {
  const formData = new FormData();
  formData.append('file', gifToUpload);
  return fetch(`https://upload.giphy.com/v1/gifs?api_key=${API_KEY}&${USERNAME}`, {
    method: 'POST',
    body: formData,
  });
};

/**
* getMyGifs - Fetches the user's uploaded GIFs from local storage.
* @author Radoslav Dimitrov <radosl.dimitrov@gmail.com>
* @returns {Promise} An object with data about the user's uploaded GIFs.
*/
export const getMyGifs = async () => {
  const myGifsIds = localStorage.getItem(MY_UPLOADS);
  if (myGifsIds) {
    const data = await fetch(`https://api.giphy.com/v1/gifs?api_key=${API_KEY}&ids=${myGifsIds}`);
    const myGifs = await data.json();
    return myGifs;
  }
  return [];
};

/**
 * searchGiphy - Fetch data from API, a search giphy
 * @author Georgi Tsekleov <jorkata99@abv.bg>
 * @returns {Promise}  Object with data about a search giphy
 */
export const searchGiphy = async (q) => {
  const query = await fetch(`https://api.giphy.com/v1/gifs/search?api_key=${API_KEY}&q=${q}&limit=20&offset=0&rating=g&lang=en`);
  const data = await query.json();
  return data;
};

/**
 * getRandomGiphy Fetch data from API, a random giphy
 * @author Nikolay Tsvetkov <nikolay.val.tsvetkov@gmail.com>
 * @returns {Promise}  Object with data about a random giphy
 */
export const getRandomGiphy = async () => {
  const query = await fetch(`https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}&tag=&rating=g`);
  const data = await query.json();
  return data;
};

/**
* localStorageLengthCheck - Checks if there are any items in the localStorage
* and displays "My Uploads" button if there are any items in the localStorage.
* @author Georgi Tsekleov <jorkata99@abv.bg>
* @returns {void}
*/
export const localStorageLengthCheck = () => {
  if (localStorage.length > 0) {
    document.getElementById('my-uploads-btn').style.display = 'block';
  }
};

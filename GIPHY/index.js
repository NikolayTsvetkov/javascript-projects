import {
  ABOUT, FAVORITES, HOME, MY_UPLOADS,
} from './src/common/constants.js';
import {
  getGiphyById, getMyGifs, getTrendingGifs, localStorageLengthCheck, uploadGif,
} from './src/requests/queries.js';
import showFavouritesState from './src/utils/favouriteView.js';
import toAboutView from './src/views/about-view.js';
import renderSingleGiphyDetails from './src/views/renderSingleGiphyDetails.js';
import { searchFunction, searchShowHide } from './src/views/search-show-hide.js';
import renderTrendingView from './src/views/trending-gifs.js';

document.addEventListener('DOMContentLoaded', async () => {
  const trendingGifs = await getTrendingGifs();
  const giphyContainer = document.getElementById('outer-content-div-test');
  renderTrendingView(trendingGifs, giphyContainer);
  localStorageLengthCheck();

  document.addEventListener('click', async (event) => {
    /**
    * Shows the details of a single giphy when its element is hover on.
    * Hides the "details" button initially when its element is hover off.
    * @param {Object} event - The event object.
    * @returns {Object}  Object with detail data about the giphy.
    */
    if (event.target.hasAttribute('data-giphy-id')) {
      const id = event.target.getAttribute('data-giphy-id');
      const giphyById = await getGiphyById(id);
      giphyContainer.innerHTML = renderSingleGiphyDetails(giphyById.data);
    }

    if (event.target.hasAttribute('data-page')) {
      const page = event.target.getAttribute('data-page');
      const allNavs = document.getElementsByClassName('nav-btn');

      [...allNavs].map((nav) => {
        if (nav.getAttribute('data-page') === page) {
          return nav.classList.add('active');
        }
        return nav.classList.remove('active');
      });

      /**
      navs - function that navigates to different views based on the selected page.
      @param {string} page - The selected page to navigate to.
      @returns {void}
      */
      switch (page) {
        case HOME: {
          // Retrieves trending gifs from API and renders view
          const trendingGif = await getTrendingGifs();
          renderTrendingView(trendingGif, giphyContainer);
          // Show or hide search bar based on page
          searchShowHide(0);
          break;
        }
        case FAVORITES: {
          // Retrieves favorite gifs from local storage and renders view
          const favs = JSON.parse(window.localStorage.getItem(FAVORITES));
          showFavouritesState(favs, giphyContainer);
          break;
        }
        case ABOUT: {
          // Renders about view
          giphyContainer.innerHTML = toAboutView();
          break;
        }
        case MY_UPLOADS: {
          // Retrieves user's uploaded gifs from API and renders view
          const myGifs = await getMyGifs();
          renderTrendingView(myGifs, giphyContainer);
          break;
        }

        default: {
          // If invalid page is selected, navigates to home page
          const trendingGif = await getTrendingGifs();
          renderTrendingView(trendingGif, giphyContainer);
          searchShowHide(0);
          break;
        }
      }
    }

    if (event.target.hasAttribute('data-heart-icon')) {
      if (window.localStorage.getItem(FAVORITES)) {
        let favs = JSON.parse(window.localStorage.getItem(FAVORITES));
        if (favs.some((id) => id === event.target.getAttribute('data-heart-icon'))) {
          favs = favs.filter((id) => id !== event.target.getAttribute('data-heart-icon'));
        } else {
          favs.push(event.target.getAttribute('data-heart-icon'));
        }

        window.localStorage.setItem(FAVORITES, JSON.stringify(favs));
      } else {
        window.localStorage.setItem(FAVORITES, JSON.stringify([event.target.getAttribute('data-heart-icon')]));
      }
      const giphys = await getTrendingGifs();

      renderTrendingView(giphys, giphyContainer);
      const allNavs = document.getElementsByClassName('nav-btn');

      [...allNavs].map((nav) => {
        if (nav.getAttribute('data-page') === HOME) {
          return nav.classList.add('active');
        }
        return nav.classList.remove('active');
      });
    }
  });

  document.addEventListener('input', async (event) => {
    searchFunction(event, trendingGifs, giphyContainer);
  });

  // upload GIF
  const inputEl = document.getElementById('myFileInput');
  if (inputEl) {
    inputEl.addEventListener('change', async (event) => {
      const fileToUpload = event.target.files[0];
      try {
        const response = await uploadGif(fileToUpload);
        const responseJSON = await response.json();
        const myExistingGifs = localStorage.getItem(MY_UPLOADS);
        if (myExistingGifs) {
          localStorage.setItem(MY_UPLOADS, `${myExistingGifs},${responseJSON.data.id}`);
        } else {
          localStorage.setItem(MY_UPLOADS, responseJSON.data.id);
        }
        localStorageLengthCheck();
      } catch (e) {
        console.error(e);
      }
    });
  }
});

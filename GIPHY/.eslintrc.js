module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: 'airbnb-base',
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    'no-param-reassign': 'off',
    'no-use-before-define': 'off',
    'no-multi-assign': 'off',
    'no-constant-condition': 'off',
    'no-cond-assign': 'off',
    'import/extensions': 'off',
    'consistent-return': 'off',
    'dot-notation': 'off',
    'no-console': 'off',
    'array-callback-return': 'off',
    'no-await-in-loop': 'off',
  },
};

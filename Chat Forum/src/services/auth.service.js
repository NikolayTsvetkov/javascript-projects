import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, getAuth } from 'firebase/auth';
// import { auth } from '../config/firebase-config';
const auth = getAuth();

export const registerUser = async (email, password) => {
    console.log(email, password);

    await createUserWithEmailAndPassword(auth, email, password).then(snapshot => {
        console.log(snapshot);

    })
};

export const loginUser = (email, password) => {
    return signInWithEmailAndPassword(auth, email, password);
};

export const logoutUser = () => {
    return signOut(auth);
};

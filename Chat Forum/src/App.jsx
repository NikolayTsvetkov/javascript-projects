import Landing from "./components/Landing/Landing"
import Login from "./components/Login/Login"
import SignUpForm from "./components/SignUp/SignupForm"
import { Route, Routes, NavLink } from "react-router-dom"

const App = () => {


  return (
    <>
      <Routes>
        <Route path="/" element={<Landing />}></Route>
        <Route path="/signup" element={<SignUpForm />} />
        <Route path="/login" element={<Login />} />
      </Routes>



    </>

  )
}

export default App

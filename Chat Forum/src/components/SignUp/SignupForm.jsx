
import { FormControl, FormLabel, Card, Center, CardBody, Stack, Button, Input } from '@chakra-ui/react'
import { useState } from 'react'
import { createUserWithEmailAndPassword } from 'firebase/auth'
import "./SignupForm.css"
// import { createUserObject } from '../../config/firebase-config'
import { useAuth } from '../../contexts/AuthContext'
import { registerUser } from '../../services/auth.service'
// import { Link, useHistory } from "react-router-dom"

const inputFields = {
  username: '',
  password: '',
  firstName: '',
  lastName: '',
  email: ''
}
export default function SignUpForm() {

  const [fields, setFields] = useState({ inputFields })
  const { username, password, firstName, lastName, email } = fields
  console.log(fields)
  const { signup } = useAuth()
  // const history = useHistory()

  const handleSubmit = async (e) => {
    e.preventDefault()

    try {
      const { user } = await (createUserWithEmailAndPassword(email, password))
      console.log(user)
      // history.push("/")
    } catch (err) {
      console.log("Something went wrong ", err)
    }
  }
  const handleChange = (event) => {
    const { name, value } = event.target
    setFields({ ...fields, [name]: value })
  }

  const registerUsers = async (e) => {
    e.preventDefault()
    console.log(fields)
    await registerUser(fields?.email, fields?.password)
  }

  return (
    <>
      <form onSubmit={registerUsers}>


        <h2>Sign up here</h2>
        <Card className="loginContainer" pt={'40px'}>
          <CardBody>

            <Stack spacing={5}>
              <FormControl isRequired>
                <FormLabel>Username</FormLabel>
                <Input onChange={handleChange} name="username" value={username} />
              </FormControl>

              <FormControl isRequired>
                <FormLabel>Password</FormLabel>
                <Input onChange={handleChange} name="password" type={'password'} value={password} />
              </FormControl>
              <FormControl isRequired>
                <FormLabel>First Name</FormLabel>
                <Input onChange={handleChange} name="firstName" value={firstName} />
              </FormControl>
              <FormControl isRequired>
                <FormLabel>Last Name</FormLabel>
                <Input onChange={handleChange} name="lastName" value={lastName} />
              </FormControl>
              <FormControl isRequired>
                <FormLabel>E-mail</FormLabel>
                <Input onChange={handleChange} type={"email"} name="email" value={email} />
              </FormControl>

              <Center><Button type='submit' colorScheme='blue' size="xs" width={'50%'} alignItems={"center"}>Sign up</Button></Center>

              <Button onClick={registerUsers}>show</Button>
            </Stack>
          </CardBody>
        </Card>
      </form>

    </>



  )
}

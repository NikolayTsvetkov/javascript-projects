# Photography Forum App

## Description
This project is a photography forum app that allows users to engage in discussions, share pictures, and interact with other photographers.

## Features

   <ul>
  <li>Picture Upload: Users can upload their own pictures to share with the community.</li>
  <li>Gallery: Users can browse through a gallery of pictures shared by other members.</li>
  <li>Post Creation: Users can create posts to start discussions or share their thoughts.</li>
  <li>Post Interaction: Users can like posts and create comments to engage with the content.</li>
  <li>Admin Panel: The app includes an admin panel with search options for managing users and posts.</li>
  <li>User Blocking: Admins have the ability to block certain users from accessing the app.</li>
  <li>Post Deletion: Admins can delete posts that violate community guidelines or are deemed inappropriate.</li>
</ul>

## Technologies Used

    JavaScript
    React
    ChakraUI
    Firebase